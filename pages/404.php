<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once INCLUDES_DIR . '/head.php'; ?>
</head>
<body>
<?php Utils::showLoadingDots(7); ?>
<?php include_once INCLUDES_DIR . '/header.php'; ?>
<div class="tt-breadcrumb">
	<div class="container">
		<ul>
			<li><a href="<?php echo $route->site('/', false); ?>">Início</a></li>
			<li>Página não encontrada</li>
		</ul>
	</div>
</div>
<div id="tt-pageContent">
    <div class="tt-offset-0 container-indent">
        <div class="tt-page404">
            <h1 class="tt-title">PÁGINA NÃO ENCONTRADA.</h1>
            <p>Está página não foi encontrada.</p>
            <a href="<?php echo $route->site('/', false); ?>" class="btn">VOLTAR</a>
        </div>
    </div>
</div>
<?php include_once INCLUDES_DIR . '/footer.php'; ?>
<?php include_once INCLUDES_DIR . '/modal.php'; ?>

<?php include_once INCLUDES_DIR . '/footer.php'; ?>
<?php include_once INCLUDES_DIR . '/modal.php'; ?>
<?php include_once INCLUDES_DIR . '/foot.php'; ?>
</body>
</html>