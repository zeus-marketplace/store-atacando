<?php

if(isset($_SESSION['site']['logged']) && $_SESSION['site']['logged']){
    header('Location: ' . $route->site('/', false));
    die();
} elseif($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['action']) && $_POST['action'] === 'login'){

        if(!isset($_POST['document']) || empty($_POST['document'])){
            $error = "Informe um CPF ou um CNPJ válido";
        } elseif(!isset($_POST['password']) || empty($_POST['password'])){
            $error = "Informe sua senha";
        } else {
            $document = Utils::onlyNumber($_POST['document']);
            $password = $_POST['password'];
            if(strlen($document) === 11){
                $process = $login->doSiteLogin($CurrentStore->storeId, 'cpf', $document, $password);
            } elseif(strlen($document) === 14){
                $process = $login->doSiteLogin($CurrentStore->storeId, 'cnpj', $document, $password);
            } else {
                $error = "Informe um CPF ou um CNPJ válido";
            }

            if($process instanceof CustomerDAO || $process instanceof CompanyDAO){
                header('Location: ' . $route->site('/', false));
                exit;
            } else {
                $error = $process['message'];
            }
        }

    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once INCLUDES_DIR . '/head.php'; ?>
</head>
<body>
<?php Utils::showLoadingDots(7); ?>
<?php include_once INCLUDES_DIR . '/header.php'; ?>
<div class="tt-breadcrumb">
	<div class="container">
		<ul>
			<li><a href="<?php echo $route->site('/', false); ?>">Início</a></li>
			<li>Entrar</li>
		</ul>
	</div>
</div>
<div id="tt-pageContent">
	<div class="container-indent">
		<div class="container">
			<h1 class="tt-title-subpages noborder">ACESSAR</h1>
			<div class="tt-login-form">
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="tt-item">
							<h2 class="tt-title">NOVO POR AQUI?</h2>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eius laboriosam nam nihil quae. Alias dolores doloribus est exercitationem laudantium odio omnis rerum sint soluta vel. Ab earum odio sed!
							</p>
							<div class="form-group">
								<a href="<?php echo $route->site('/cadastrar'); ?>" class="btn btn-top btn-border">CRIAR CONTA</a>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="tt-item">
							<h2 class="tt-title">ENTRAR</h2>
							Se você tem uma conta conosco, por favor faça login.
							<div class="form-default form-top">
                                <?php if(isset($error)): ?>
                                    <div class="alert alert-danger">
                                        <?php echo $error; ?>
                                    </div>
                                <?php endif; ?>
								<form id="customer_login" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" novalidate="novalidate">
                                    <input type="hidden" name="action" value="login">
									<div class="form-group">
										<label for="login-document">CPF/CNPJ *</label>
										<div class="tt-required">* Obrigatório</div>
                                        <?php $document_storage = $_POST['document'] ?? $_GET['document'] ?? $_COOKIE['document'] ?? ''; ?>
										<input type="text" name="document" class="form-control input-mask mask-cpf-cnpj" id="login-document" placeholder="Informe seu CPF/CNPJ" value="<?php echo $document_storage; ?>">
									</div>
									<div class="form-group">
										<label for="login-password">SENHA *</label>
										<input type="password" name="password" class="form-control" id="login-password" placeholder="Informe sua senha" <?php echo $document_storage? 'autofocus': ''; ?>>
									</div>
									<div class="row">
										<div class="col-auto mr-auto">
											<div class="form-group">
												<button class="btn btn-border" type="submit">ENTRAR</button>
											</div>
										</div>
										<div class="col-auto align-self-end">
											<div class="form-group">
												<ul class="additional-links">
													<li><a href="javascript:void(0);">Recuperar acesso</a></li>
												</ul>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once INCLUDES_DIR . '/footer.php'; ?>
<?php include_once INCLUDES_DIR . '/modal.php'; ?>
<?php ob_start(); ?>
    <script>
    $masks = $(".input-mask");
    $masks.filter(".mask-date").mask("00/00/0000", {placeholder: '__/__/____'});
    $masks.filter(".mask-cpf").mask('000.000.000-00', {reverse: true, placeholder: '___.___.___-__'});
    $masks.filter(".mask-cnpj").mask('00.000.000/0000-00', {reverse: true, placeholder: '__.___.___/____-__'});

    $masks.filter(".mask-cpf-cnpj").mask('000.000.000-009', {
        placeholder: '___.___.___-__',
        onKeyPress: function(document, e, field, options) {
            var raw = document.replace(/[^0-9]+/g, '');
            var masks = ['000.000.000-009', '00.000.000/0000-00'];
            var mask = raw.length <= 11? masks[0] : masks[1];
            console.log(mask, field);
            field.mask(mask, options);
        }
    });
    $masks.filter(".mask-phone").mask('(00) 0000-0000', {placeholder: '(__) ____-____'});
    $masks.filter(".mask-cellphone").mask('(00) 0 0000-0000', {placeholder: '(__) 9 ____-____'});
    var inputsMoney = $masks.filter(".mask-money");
    setMoneyMaskField(inputsMoney);

    function setMoneyMaskField(field){
        field.on('focus', function () {
            if(!this.value || this.value === ''){
                this.value = applyMoneyMask(this.value);
            }
        });
        field.mask("#.##0,00", {
            reverse: true,
            onKeyPress: function(value, event, currentField, options){
                value = value.replace(/[^0-9]+/g, '').replace(/^0+/, '');
                if(typeof value ==='string' && value.length <=3 ){
                    value = applyMoneyMask(value);
                    currentField.val(value);
                    currentField.mask("#.##0,00", options);
                }
            }
        });
    }

    window.setMoneyMaskField = setMoneyMaskField;

    function applyMoneyMask(value) {
        return ('000' + value).slice(-3).replace(/(.*?)([0-9]{2})$/, '$1,$2');
    }
</script>
<?php
    $custom_script = ob_get_contents();
ob_end_clean();
array_push($scripts,
    '<script src="assets/js/jquery.mask.min.js"></script>',
    $custom_script
);
?>
<?php include_once INCLUDES_DIR . '/foot.php'; ?>
</body>
</html>