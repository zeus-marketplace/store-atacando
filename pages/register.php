<?php

if(isset($_SESSION['site']['logged']) && $_SESSION['site']['logged']){
    header('Location: ' . $route->site('/', false));
    die();
} elseif($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['action']) && $_POST['action'] === 'add-client'){

    try {
        if(!isset($_POST['register_document']) || empty($_POST['register_document'])){
            throw new Exception("Informe um CPF ou um CNPJ válido");
        } elseif(!isset($_POST['register_password']) || empty($_POST['register_password'])){
            throw new Exception("Informe sua senha");
        } elseif(!isset($_POST['register_conf_password']) || $_POST['register_password'] !== $_POST['register_conf_password']){
            throw new Exception("As senhas não batem");
        }

        $document = Utils::onlyNumber($_POST['register_document']);
        if(strlen($document) === 11){
            $type = 'customer';
            if(!isset($_POST['register_first_name']) || empty($_POST['register_first_name'])){
                throw new Exception("Informe seu nome");
            } elseif(!isset($_POST['register_last_name']) || empty($_POST['register_last_name'])){
                throw new Exception("Informe seu sobrenome");
            } elseif(isset($_POST['register_birthdate']) && !empty($_POST['register_birthdate']) && !Utils::isValidDate($_POST['register_birthdate'], 'd/m/Y')){
                throw new Exception("Informe uma data de nascimento válida");
            } elseif(!isset($_POST['register_cellphone']) || empty($_POST['register_cellphone'])){
                throw new Exception("Informe seu celular");
            } elseif(!isset($_POST['register_email']) || empty($_POST['register_email'])){
                throw new Exception("Informe seu e-mail");
            } elseif(!Utils::isValidEmail($_POST['register_email'])){
                throw new Exception("Informe um e-mail válido");
            }
        } elseif(strlen($document) === 11){
            $type = 'company';
            if(!isset($_POST['register_social_name']) || empty($_POST['register_social_name'])){
                throw new Exception("Informe a Razão Social");
            } elseif(!isset($_POST['register_fantasy_name']) || empty($_POST['register_fantasy_name'])){
                throw new Exception("Informe o Nome Fantasia");
            } elseif(!isset($_POST['register_phone']) || empty($_POST['register_phone'])){
                throw new Exception("Informe seu telefone fixo");
            }
        } else {
            throw new Exception("CPF ou CNPJ inválido");
        }

        $client_email = $_POST['register_email'];
        $client_password = md5($_POST['register_password']);
        $store_id = $CurrentStore->storeId;
        $data = (object) [
            'store_id' => $store_id,
            'client_document' => $document,
            'active' => 1
        ];
        $data->same_address = true;
        if($type === 'customer'){

            $customer_first_name = $_POST['register_first_name'];
            $customer_last_name = $_POST['register_last_name'];
            $customer_birth_date = $_POST['register_birthdate'];
            $customer_genre = $_POST['register_genre'];
            $customer_cellphone = $_POST['register_cellphone'];

            $data->customer_email = $client_email;
            $data->customer_password = $client_password;
            $data->customer_first_name = $customer_first_name;
            $data->customer_last_name = $customer_last_name;
            $data->customer_birth_date = $customer_birth_date;
            $data->customer_genre = $customer_genre;
            $data->customer_cellphone = $customer_cellphone;

            $data->customer_payment_address['zip_code'] = null;
            $data->customer_payment_address['street'] = null;
            $data->customer_payment_address['number'] = null;
            $data->customer_payment_address['street_extra'] = null;
            $data->customer_payment_address['district'] = null;
            $data->customer_payment_address['county'] = null;
            $data->customer_payment_address['state'] = null;

            $data->customer_shipping_address['zip_code'] = null;
            $data->customer_shipping_address['street'] = null;
            $data->customer_shipping_address['number'] = null;
            $data->customer_shipping_address['street_extra'] = null;
            $data->customer_shipping_address['district'] = null;
            $data->customer_shipping_address['county'] = null;
            $data->customer_shipping_address['state'] = null;

            $process = $Client->createCustomer($data);

        } elseif(strlen($document) === 14){

            $company_social_name = $_POST['customer_social_name'];
            $company_fantasy_name = $_POST['register_fantasy_name'];
            $company_phone = $_POST['register_phone'];

            $data->company_email = $client_email;
            $data->company_password = $client_password;
            $data->company_first_name = $company_social_name;
            $data->company_last_name = $company_fantasy_name;
            $data->company_phone = $company_phone;

            $data->company_payment_address['zip_code'] = null;
            $data->company_payment_address['street'] = null;
            $data->company_payment_address['number'] = null;
            $data->company_payment_address['street_extra'] = null;
            $data->company_payment_address['district'] = null;
            $data->company_payment_address['county'] = null;
            $data->company_payment_address['state'] = null;

            $data->company_shipping_address['zip_code'] = null;
            $data->company_shipping_address['street'] = null;
            $data->company_shipping_address['number'] = null;
            $data->company_shipping_address['street_extra'] = null;
            $data->company_shipping_address['district'] = null;
            $data->company_shipping_address['county'] = null;
            $data->company_shipping_address['state'] = null;

            $process = $Client->createCompany($data);

        } else {
            throw new Exception("Informe um CPF ou um CNPJ válido");
        }

        if($process instanceof CustomerDAO || $process instanceof CompanyDAO){
            $login->doSiteLogin($store_id, $type === 'customer'? 'cpf': 'cnpj', $document, $_POST['register_password']);
            header('Location: ' . $route->site('/', false));
            exit;
        } else {
            throw new Exception($process['message']);
        }
    } catch (Exception $exception) {
        $error = $exception->getMessage();
    } catch (Error $exception) {
        $error = $exception->getMessage();
    }

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once INCLUDES_DIR . '/head.php'; ?>
</head>
<body>
<?php Utils::showLoadingDots(7); ?>
<?php include_once INCLUDES_DIR . '/header.php'; ?>
<div class="tt-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="<?php echo $route->site('/', false); ?>">Início</a></li>
            <li>Entrar</li>
        </ul>
    </div>
</div>
<div id="tt-pageContent">
    <div class="container-indent">
        <div class="container">
            <h1 class="tt-title-subpages noborder">CRIAR CONTA</h1>
            <div class="tt-login-form">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6">
                        <div class="tt-item">
                            <div class="form-default">
                                <?php if(isset($error)): ?>
                                    <div class="alert alert-danger text-danger">
                                        <?php echo $error; ?>
                                    </div>
                                <?php endif; ?>
                                <form id="register-form" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" novalidate="novalidate">
                                    <input type="hidden" name="action" value="add-client">
                                    <div class="form-group verify">
                                        <label for="register_document">CPF/CNPJ</label>
                                        <div class="tt-required">* Obrigatório</div>
                                        <input type="text" name="register_document" class="form-control input-mask mask-cpf-cnpj" id="register_document" placeholder="Informe seu CPF/CNPJ">
                                        <div class="d-flex justify-content-end mt-2">
                                            <button type="submit" class="verify-btn btn btn-primary btn-small mr-0">
                                                Verificar
                                            </button>
                                        </div>
                                    </div>

                                    <div class="fields <?php echo !isset($_POST['action'])? 'd-none': ''; ?>">

                                        <div id="customer-fields" class="custom-fields d-none">
                                            <div class="form-group">
                                                <label for="register_first_name">NOME *</label>
                                                <input type="text" name="register_first_name" class="form-control" id="register_first_name" placeholder="Informe seu nome">
                                            </div>
                                            <div class="form-group">
                                                <label for="register_last_name">SOBRENOME *</label>
                                                <input type="text" name="register_last_name" class="form-control" id="register_last_name" placeholder="Informe seu sobrenome">
                                            </div>
                                            <div class="form-group">
                                                <label for="register_birthdate">DATA DE NASCIMENTO</label>
                                                <input type="text" name="register_birthdate" class="form-control input-mask mask-date" id="register_birthdate" placeholder="Informe sua data de nascimento">
                                            </div>
                                            <div class="form-group">
                                                <label>SEXO *</label>
                                                <ul class="list-form-inline">
                                                    <li>
                                                        <label class="radio">
                                                            <input id="register_genre_male" type="radio" name="register_genre" data-container-errors="#register_genre-errors" value="male">
                                                            <span class="outer"><span class="inner"></span></span>
                                                            Masculino
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="radio">
                                                            <input id="register_genre_female" type="radio" name="register_genre" value="female">
                                                            <span class="outer"><span class="inner"></span></span>
                                                            Feminino
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="radio">
                                                            <input id="register_genre_other" type="radio" name="register_genre" value="other">
                                                            <span class="outer"><span class="inner"></span></span>
                                                            Outro
                                                        </label>
                                                    </li>
                                                </ul>
                                                <div id="register_genre-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="register_cellphone">CELULAR *</label>
                                                <input type="text" name="register_cellphone" class="form-control input-mask mask-cellphone" id="register_cellphone" placeholder="Informe seu telefone">
                                            </div>
                                        </div>

                                        <div id="company-fields" class="custom-fields d-none">
                                            <div class="form-group">
                                                <label for="register_social_name">RAZÃO SOCIAL *</label>
                                                <input type="text" name="register_social_name" class="form-control" id="register_social_name" placeholder="Informe a Razão Social">
                                            </div>
                                            <div class="form-group">
                                                <label for="register_fantasy_name">Nome Fantasia *</label>
                                                <input type="text" name="register_fantasy_name" class="form-control" id="register_fantasy_name" placeholder="Informe o Nome Fantasia">
                                            </div>
                                            <div class="form-group">
                                                <label for="register_phone">TELEFONE FIXO *</label>
                                                <input type="text" name="register_phone" class="form-control input-mask mask-phone" id="register_phone" placeholder="Informe um telefone fixo">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="register_email">E-MAIL *</label>
                                            <input type="text" name="register_email" class="form-control" id="register_email" placeholder="Informe seu e-mail">
                                        </div>
                                        <div class="form-group">
                                            <label for="register_password">SENHA *</label>
                                            <input type="password" name="register_password" class="form-control" id="register_password" placeholder="Informe sua senha">
                                        </div>
                                        <div class="form-group">
                                            <label for="register_conf_password">CONFIRME SUA SENHA *</label>
                                            <input type="password" name="register_conf_password" class="form-control" id="register_conf_password" placeholder="Confirme sua senha">
                                        </div>
                                        <div class="row">
                                            <div class="col-auto">
                                                <div class="form-group">
                                                    <button class="btn btn-border" type="submit">CRIAR</button>
                                                </div>
                                            </div>
                                            <div class="col-auto align-self-center">
                                                <div class="form-group">
                                                    <ul class="additional-links">
                                                        <li>ou <a href="#">Volte à loja</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="linking d-none flex-column">
                                        <h5 class="text-center exists-title">
                                            Encontramos seu cadastro.<br>Deseja vinculá-lo a nossa loja?
                                        </h5>
                                        <div class="form-group">
                                            <label for="linking_password">SENHA *</label>
                                            <input type="password" name="linking_password" class="form-control" id="linking_password" placeholder="Informe sua senha">
                                        </div>
                                        <button type="button" class="linking-btn btn btn-primary btn-small mr-0">
                                            Vincular
                                        </button>
                                    </div>
                                    <div class="exists d-none flex-column">
                                        <h5 class="text-center exists-title">
                                            Você já está cadastrado em nossa loja.
                                        </h5>
                                        <a href="<?php echo $route->site('/entrar'); ?>" class="btn btn-primary btn-small goto-login">Entrar</a>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once INCLUDES_DIR . '/footer.php'; ?>
<?php include_once INCLUDES_DIR . '/modal.php'; ?>
<?php ob_start(); ?>
<script>
    $masks = $(".input-mask");
    $masks.filter(".mask-date").mask("00/00/0000", {placeholder: '__/__/____'});
    $masks.filter(".mask-cpf").mask('000.000.000-00', {reverse: true, placeholder: '___.___.___-__'});
    $masks.filter(".mask-cnpj").mask('00.000.000/0000-00', {reverse: true, placeholder: '__.___.___/____-__'});

    $masks.filter(".mask-cpf-cnpj").mask('000.000.000-009', {
        placeholder: '___.___.___-__',
        onKeyPress: function(document, e, field, options) {
            var raw = document.replace(/[^0-9]+/g, '');
            var masks = ['000.000.000-009', '00.000.000/0000-00'];
            var mask = raw.length <= 11? masks[0] : masks[1];
            console.log(mask, field);
            field.mask(mask, options);
        }
    });
    $masks.filter(".mask-phone").mask('(00) 0000-0000', {placeholder: '(__) ____-____'});
    $masks.filter(".mask-cellphone").mask('(00) 0 0000-0000', {placeholder: '(__) 9 ____-____'});
    var inputsMoney = $masks.filter(".mask-money");
    setMoneyMaskField(inputsMoney);

    function setMoneyMaskField(field){
        field.on('focus', function () {
            if(!this.value || this.value === ''){
                this.value = applyMoneyMask(this.value);
            }
        });
        field.mask("#.##0,00", {
            reverse: true,
            onKeyPress: function(value, event, currentField, options){
                value = value.replace(/[^0-9]+/g, '').replace(/^0+/, '');
                if(typeof value ==='string' && value.length <=3 ){
                    value = applyMoneyMask(value);
                    currentField.val(value);
                    currentField.mask("#.##0,00", options);
                }
            }
        });
    }

    window.setMoneyMaskField = setMoneyMaskField;

    function applyMoneyMask(value) {
        return ('000' + value).slice(-3).replace(/(.*?)([0-9]{2})$/, '$1,$2');
    }

    var registerForm = $('#register-form');
    var baseURL = <?php echo json_encode([
        'customer' => $route->admin('/api/clientes/pessoa-fisica/'),
        'company' => $route->admin('/api/clientes/pessoa-juridica/')
    ]); ?>;
    registerForm.on('submit', function (e) {
        console.log(registerForm.data('linking'));
        if(registerForm.data('linking')){
            e.preventDefault();
            linkingWithStore(registerForm.data('url'), true);
        } else if(!registerForm.data('verified')){
            e.preventDefault();
            var documentField = $('#register_document');
            var document = documentField.val().replace(/[^0-9]+/g, '');
            var result = verifyDocument(document);
            if(result){
                var type = document.length === 11? 'customer': 'company';
                var url = type === 'customer'? baseURL.customer: baseURL.company;
                $.ajax({
                    type: 'get',
                    url: url + '/verificar/' + document,
                    cache: false,
                    data: {
                        store_id: '<?php echo $CurrentStore->storeId; ?>'
                    },
                    success: function (response) {
                        registerForm.data('verified', true);
                        documentField.prop('readonly', true);
                        if(response.exists){
                            $('.verify').addClass('d-none');
                            if(response.linked) {
                                $('.exists').removeClass('d-none').addClass('d-flex');
                                $('.verify-btn').addClass('d-none');
                                $('.goto-login').attr('href', '<?php echo $route->site('/entrar');?>?document=' + document);
                            } else {
                                registerForm.data('linking', true);
                                registerForm.data('url', url + '/vincular/' + document);
                                $('.linking').removeClass('d-none').addClass('d-flex');
                                $('.linking-btn').on('click', linkingWithStore(url + '/vincular/' + document));
                            }
                        } else {
                            $('.custom-fields ').addClass('d-none').find(':input').prop('disabled', true);
                            if(type === 'customer'){
                                $('#customer-fields').removeClass('d-none').find(':input').prop('disabled', false);
                            } else {
                                $('#company-fields').removeClass('d-none').find(':input').prop('disabled', false);
                            }
                            validateForm();
                            $('.verify-btn').addClass('d-none');
                            $('.fields').removeClass('d-none');
                        }
                    },
                    error: function (request, error) {
                        console.log(request, error);
                    }
                });
            }
        }
    });

    function linkingWithStore(url, autorun){
        var fn = function () {
            var password = $('#linking_password').val();
            $.ajax({
                type: 'post',
                url: url,
                cache: false,
                data: {
                    store_id: '<?php echo $CurrentStore->storeId; ?>',
                    password: password
                },
                success: function (response) {
                    $.post('<?php echo $route->site('/entrar', false); ?>', {
                        document: response.document,
                        password: password,
                        action: 'login'
                    }, function () {
                        window.location.href = '<?php echo $route->site('/', false); ?>';
                    });
                },
                error: function (request, error) {
                    console.log(request, error);
                }
            })
        };
        if(autorun){
            return fn();
        }
        return fn;
    }

    function verifyDocument(document){
        if(document.length === 11){
            return isValidCPF(document);
        } else if(document.length === 14) {
            return isValidCNPJ(document);
        } else {
            return false;
        }
    }

    function isEqual(str) {
        if (typeof str === 'number') {
            str = str.toString();
        }
        return str.split('').filter(function (letter, index, self) {
            return self.indexOf(letter) === index;
        }).length === 1;

    }

    function isValidCPF(document) {
        var cpf = document.replace(/[^0-9]/g, '')
            , compareCPF = cpf.substring(0, 9)
            , add = 0
            , i, u
        ;

        if (cpf.length < 11 || isEqual(cpf)) {
            return false;
        }

        for (i = 8, u = 2; i >= 0; i-- , u++) {
            add = add + parseInt(cpf.substring(i, i + 1)) * u;
        }

        compareCPF = compareCPF + ((add % 11) < 2 ? 0 : 11 - (add % 11));
        add = 0;

        for (i = 9, u = 2; i >= 0; i-- , u++) {
            add = add + parseInt(cpf.substring(i, i + 1)) * u;
        }

        compareCPF = compareCPF + ((add % 11) < 2 ? 0 : 11 - (add % 11));

        return compareCPF.toString() === cpf;
    }

    function isValidCNPJ(document) {
        var cnpj = document.replace(/[^0-9]/g, '')
            , len = cnpj.length - 2
            , numbers = cnpj.substring(0, len)
            , digits = cnpj.substring(len)
            , add = 0
            , pos = len - 7
            , result
        ;


        if (cnpj.length < 11 || isEqual(cnpj)) {
            return false;
        }

        for (var i = len; i >= 1; i--) {
            add = add + parseInt(numbers.charAt(len - i)) * pos--;
            if (pos < 2) { pos = 9; }
        }

        result = (add % 11) < 2 ? 0 : 11 - (add % 11);
        if (result.toString() !== digits.charAt(0)) {
            return false;
        }

        len = len + 1;
        numbers = cnpj.substring(0, len);
        add = 0;
        pos = len - 7;

        for (i = 13; i >= 1; i--) {
            add = add + parseInt(numbers.charAt(len - i)) * pos--;
            if (pos < 2) { pos = 9; }
        }

        result = (add % 11) < 2 ? 0 : 11 - (add % 11);
        return result.toString() === digits.charAt(1);
    }

</script>
<?php
$custom_script = ob_get_contents();
ob_end_clean();
array_push($scripts,
    '<script src="assets/js/jquery.mask.min.js"></script>',
    $custom_script
);
?>
<?php include_once INCLUDES_DIR . '/foot.php'; ?>
<script>

    $.validator.addMethod("checkDocument", function(value, element) {
        var document = value.replace(/[^0-9]+/g, '');
        return (document.length === 11 && isValidCPF(document)) || (document.length === 14 && isValidCNPJ(value));
    }, "* Amount must be greater than zero");

    function validateForm(){
        registerForm.validate({
            rules: {
                register_document: {
                    required: true,
                    checkDocument: true
                },
                register_first_name: {
                    required: true,
                    minlength: 2
                },
                register_last_name: {
                    required: true,
                    minlength: 2
                },
                register_email: {
                    required: true,
                    email: true
                },
                register_genre: {
                    required: true
                },
                register_cellphone: {
                    required: true
                },
                register_password: {
                    required: true,
                    equalTo: '#register_conf_password'
                },
                register_conf_password: {
                    required: true,
                    equalTo: '#register_password'
                }
            },
            messages: {
                register_document: {
                    required: "Informe um CPF/CNPJ válido",
                    checkDocument: "O CPF/CNPJ não é válido"
                },
                register_first_name: {
                    required: "Informe um nome válido",
                    minlength: "Um nome válido tem pelo menos 2 caracteres"
                },
                register_last_name: {
                    required: "Informe um sobrenome válido",
                    minlength: "Um sobrenome válido tem pelo menos 2 caracteres"
                },
                register_email: {
                    required: "Informe um e-mail válido"
                },
                register_genre: {
                    required: "Informe seu sexo"
                },
                register_cellphone: {
                    required: "Informe seu celular"
                },
                register_password: {
                    required: "Informe sua senha",
                    equalTo: 'As senhas não conferem'
                },
                register_conf_password: {
                    required: "Informe sua senha",
                    equalTo: 'As senhas não conferem'
                },
            },
            errorPlacement: function (error, element) {
                var container = element.data("container-errors");
                if(container){
                    $(container).append(error);
                } else {
                    $(element).after(error);
                }
            }
        });
    }
    validateForm();
</script>
</body>
</html>