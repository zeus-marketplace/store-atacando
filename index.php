<?php

    session_start();

    ini_set('display_errors', true);
    ini_set('display_startup_errors', true);
    error_reporting(E_ALL);
    ini_set('error_reporting', E_ALL);

    $sep = DIRECTORY_SEPARATOR;

    if(file_exists('config/server.php')){
        require_once 'config/server.php';
    }
    require_once 'config/constants.php';
    require_once 'config/variables.php';
    require_once 'config/imports.php';
    include_once 'config/routes.php';
    include_once 'config/groups.php';

    $scripts = array();

    $page = isset($_GET['page']) && $_GET['page'] ? $_GET['page']: 'painel';
    $pageRoute = $route->getPage($page);
    $storeSlug = $pageRoute['params']['slug'] ?? false;

    $notFoundRoute = $route->getPage('404');
    $accessDeniedRoute = $route->getPage('acesso-negado');

    $domain = $_SERVER['SERVER_NAME'];
    $domain = preg_replace('/^((https?:\/\/)?(www\.)?)?/', '', $domain);
    $CurrentStore = $Store->getByDomain($domain);
    if(!$CurrentStore) {
        header('HTTP/1.0 400 Bad Request');
        exit;
    }
    $route->isSiteRoute(true);
    $CurrentStoreInfo = $Store->getSectionData('info', $CurrentStore->storeId);

    if($pageRoute === null){
        $storePage = $Store->getBySlug($page);
        if($storePage){
            $route->setStore($storePage);
            header('Location: ' . $route->store('/entrar'));
            die();
        }
        $pageRoute = $notFoundRoute;
    }

    if(isset($pageRoute['values']['slug'])){
        $page = str_replace($pageRoute['values']['slug'], '', $page);
    }
    $route->setCurrentPage($page);
    $route->setStore($CurrentStore);

    if((!isset($pageRoute['allow']) || !$pageRoute['allow']) && !$login->isLogged){
        header('Location: ' . $route->login($page));
        die();
    } elseif($login->isLogged){
        $login->checkLogin();
    }

    $level = isset($login->user) && $login->user !== null? $login->user->level: 0;

    if($pageRoute){
        $routeLevel = $pageRoute['minLevel'] ?? 0;
        if($level < $routeLevel){
            $pageRoute = $accessDeniedRoute;
        }
        include_once $pageRoute['file'];
    }

?>
