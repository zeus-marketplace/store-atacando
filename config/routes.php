<?php
$route->setPage('404', '404.php', array('title' => 'Página não encontrada', 'allow' => true));
$route->setPage('acesso-negado', 'access-denied.php', array('title' => 'Acesso negado', 'allow' => true));
$route->setPage('home', 'home.php', array('alias' => 'index', 'allow' => true));
$route->setPage('entrar', 'login.php', array('title' => 'Entrar', 'allow' => true));
$route->setPage('cadastrar', 'register.php', array('title' => 'Cadastrar', 'allow' => true));
$route->setPage('entrar/recuperar-senha', 'lost-password.php', array('title' => 'Recuperar senha', 'allow' => true));
$route->setPage('sair', 'logout.php',  array('title' => 'Sair', 'allow' => true));
