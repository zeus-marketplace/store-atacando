<?php
if(__ZEUS_MODE__ === 'production') {

    if(!defined('ZEUS_ADMIN')){
        define('ZEUS_ADMIN', '/var/www/html/admin');
    }

    if(!defined('STORE_HOST')){
        define('SITE_HOST', 'http://magelead.com.br/');
    }

    if(!defined('SITE_DIR')){
        define('SITE_DIR', '/var/www/html/stores/magelead');
    }

    if(!defined('SITE_URL')){
        define('SITE_URL', '');
    }

} elseif(__ZEUS_MODE__ === 'production') {

    if(!defined('ZEUS_ADMIN')){
        define('ZEUS_ADMIN', 'C:\\projects\\zeus\\admin\\');
    }

    if(!defined('STORE_HOST')){
        define('SITE_HOST', 'http://magelead.magelabs/');
    }

    if(!defined('SITE_DIR')){
        define('SITE_DIR', 'C:\\projects\\zeus\\magelead');
    }

    if(!defined('SITE_URL')){
        define('SITE_URL', '');
    }

}

if(!defined('PAGES_DIR')){
    define('PAGES_DIR', SITE_DIR . '/pages');
}

if(!defined('ASSETS_URL')){
    define('ASSETS_URL', SITE_URL . 'assets');
}

if(!defined('DOCUMENT_ROOT')){
    define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT'] . '/');
}

if(!defined('INCLUDES_DIR')){
    define('INCLUDES_DIR', SITE_DIR . '/includes');
}

include_once ZEUS_ADMIN . $sep . 'config' . $sep . 'constants.php';