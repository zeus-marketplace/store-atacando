<?php

$LOGIN_GROUPS = array(
//    'user-read' => array(
//        'level' => 1,
//        'name' => 'Leitor',
//    ),
//    'user-write' => array(
//        'level' => 3,
//        'name' => 'Editor',
//    ),
    'admin' => array(
        'level' => 5,
        'name' => 'Administrador de Loja',
        'max_time' => 60 * 60 * 1
    ),
    'superadmin' => array(
        'level' => 10,
        'name' => 'Super Admin',
        'max_time' => 60 * 60 * 4
    )
);