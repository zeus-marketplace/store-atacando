<meta charset="utf-8">
<title><?php echo (isset($pageRoute['title']) ? $pageRoute['title'] . ' - ': '')  . $CurrentStoreInfo->name; ?></title>
<meta name="keywords" content="HTML5 Template">
<meta name="description" content="Wokiee - Responsive HTML5 Template">
<meta name="author" content="wokiee">
<link rel="shortcut icon" href="favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="assets/css/theme.css">