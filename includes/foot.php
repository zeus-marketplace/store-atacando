<script src="assets/external/jquery/jquery.min.js"></script>
<script src="assets/external/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/external/panelmenu/panelmenu.js"></script>
<script src="assets/external/lazyLoad/lazyload.min.js"></script>
<script src="assets/external/slick/slick.min.js"></script>
<?php foreach ($scripts as $script): ?>
    <?php echo $script; ?>
<?php endforeach; ?>
<!-- form validation and sending to mail -->
<script src="assets/js/main.js"></script>
<script src="assets/external/form/jquery.form.js"></script>
<script src="assets/external/form/jquery.validate.min.js"></script>
<script src="assets/external/form/jquery.form-init.js"></script>
